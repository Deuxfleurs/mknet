import os, time

binary_path = "/tmp/mknet-bin"
storage_path = "/tmp/mknet-store"

def exec(s): 
    if os.system(s) != 0:
        raise Exception("Command terminated with an error")
def exec_retry(s, cnt=16):
    print(s)
    for i in range(cnt):
        time.sleep(i) # this is expected to sleep before running the command to reduce the noise
        if os.system(s) == 0: return
    raise Exception("Command terminated with an error too many times")
def fn_retry(f, cnt=5):
    for i in range(cnt):
        try:
            r = f()
            return r
        except Exception as e:
            if i+1 == cnt: raise e
            log(f"failed call, retry in {i} sec")
            time.sleep(i)

def id(): return int(os.environ['ID'])
def count(): return int(os.environ['SERVER_COUNT'])
def log(*args): print(f"[{id()}/{count()} - {os.environ['HOST']}]", *args)

