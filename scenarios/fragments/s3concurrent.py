import os
from os.path import exists
from pathlib import Path
from fragments import shared, garage

s3bin = Path(os.path.dirname(__file__)) / "../../benchmarks/s3concurrent/s3concurrent"

def on_garage():
    os.environ['AWS_ACCESS_KEY_ID'] = garage.key.access_key_id
    os.environ['AWS_SECRET_ACCESS_KEY'] = garage.key.secret_access_key
    os.environ['ENDPOINT'] = "localhost:3900"

    out = Path(shared.storage_path) / "s3concurrent.csv"
    shared.log(f"launching s3concurrent ({s3bin})")
    shared.exec(f"{s3bin} > {out}")
    shared.log(f"execution done, output written to {out}")
