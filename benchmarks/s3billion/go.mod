module git.deuxfleurs.fr/Deuxfleurs/mknet/benchmarks/s3billion

go 1.16

require (
	github.com/google/uuid v1.1.1
	github.com/minio/minio-go/v7 v7.0.16
)
